document.addEventListener("DOMContentLoaded", function() {
    const Gifs = document.querySelectorAll(".card");
    let card1 = null;
    let card2 = null;
    let occurrence = 0;
    let Curr_Score = 0;
  
    for (let gif of Gifs) {
      gif.addEventListener("click", handleCardClick);
    }
  
    let startBtn = document.getElementById("start-btn");
    startBtn.addEventListener("click", Game_Start);

    let Lowest_Score = localStorage.getItem("low-score");
  
    if (Lowest_Score) {
      document.getElementById("best-score").innerText = Lowest_Score;
    }

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more

    function shuffle(array) {
      let counter = array.length;
    
      // While there are elements in the array
      while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);
    
        // Decrease counter by 1
        counter--;
    
        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
      }
    
      return array;
    }


// TODO: Implement this function!
    function handleCardClick(event) {
      if (!event.target.classList.contains("before")) return;
  
      if (!card1 || !card2) {
        if (!event.target.parentElement.classList.contains("occurred")) {
          setScore(Curr_Score + 1);
        }
        event.target.parentElement.classList.add("occurred");
        card1 = card1 || event.target.parentElement;
        if(event.target.parentElement === card1){
          card2=null;
        }else{
          card2=event.target.parentElement;
        }
      }
  
      if (card1 && card2) {  
        if (card1.children[1].children[0].src === card2.children[1].children[0].src) {
          occurrence += 2;
          card1.removeEventListener("click", handleCardClick);
          card2.removeEventListener("click", handleCardClick);
          card1 = null;
          card2 = null;
        } else {
          setTimeout(function() {
            card1.classList.remove("occurred");
            card2.classList.remove("occurred");
            card1 = null;
            card2 = null;
          }, 1000);
        }
      }
  
      if (occurrence === Gifs.length) GameOver();
    }
  
    function Game_Start() {
      setScore(0);
      start.classList.add("occurring");
      let arr = [];
      for (let i = 1; i <= Gifs.length / 2; i++) {
        arr.push(i.toString());
      }
      let shuffled = shuffle(arr.concat(arr));
  
      for (let i = 0; i < Gifs.length; i++) {
        let path = "gifs/" + shuffled[i] + ".gif";
        Gifs[i].children[1].children[0].src = path;
      }
    }
  
 
  
    function setScore(newScore) {
      Curr_Score = newScore;
      document.getElementById("score").innerText = Curr_Score;
    }
  
    function GameOver() {
      let over = document.getElementById("finish");
      let header = over.children[1];
      header.innerText = "Your score: " + Curr_Score;
      let Lowest_Score = +localStorage.getItem("low-score") || Infinity;
      if (Curr_Score < Lowest_Score) {
        localStorage.setItem("low-score", Curr_Score);
      }
      document.getElementById("finish").classList.add("game-over");
    }
  });
